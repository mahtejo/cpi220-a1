package edu.asu.sorting;
import java.io.Reader;

import edu.asu.data.IO;
import edu.asu.data.Song;
public class Sorter<T> extends IO<T>{


	
	public void finnsSort(int[] data){
		int count = 0;
		while(!isSorted(data)){
			
			int toBeSwitched = data[0];
			for(int y = count; y < data.length; y++){
				if(data[toBeSwitched] > data[y]){
					toBeSwitched = y;
				}
			}
			int temp = data[count];
			data[count] = data[toBeSwitched];
			data[toBeSwitched] = temp;
			count++;
		}
	}
	
	private boolean isSorted(int[] data){
		if(data.length <= 1){
			return true;
		}
		boolean isSorted = true;
		for(int i = 1; i < data.length; i++){
			if(data[i-1] > data[i]){
				isSorted = false;
			}
		}
		return isSorted;
	}

	@Override
	public T mapObject(String[] line) {
		
		Song<T> song = new Song<T>();
		
		 song.setTitle(line[0]);
		 song.setArtist(line[1]);
		 song.setMood(Double.parseDouble(line[2]));
		 song.setGenre(line[3]);
		 song.setEnergy(line[4]);
		 song.setLiveness(Double.parseDouble(line[5]));
		 song.setTempo(Double.parseDouble(line[6]));
		 song.setSpeechiness(Double.parseDouble(line[7]));
		 song.setAcousticness(Double.parseDouble(line[8]));
		 song.setDancebility(Double.parseDouble(line[9]));
		 song.setInstrumer(Double.parseDouble(line[10]));
		 song.setLoudness(Double.parseDouble(line[11]));
		 
		 return (T) song;
	}

	@Override
	public String getString(Object object) {
		
		object = (Song)object;				
		return object.toString();
	}
	
}
