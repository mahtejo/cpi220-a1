package edu.asu.data;

import java.util.Random;

public class DataGenerator {

	public static int[] generateRandomIntArray(int size){
		int[] data = new int[size];
		Random r = new Random();
		for(int i = 0; i < size; i++){
			data[i] = r.nextInt(size);
		}
		return data;
	}
	
}
