package edu.asu.data;

public class Song<T> {
	
	private String title;
	private String artist;
	private String genre;
	private String energy;
	private double mood;
	private double tempo;
	private double loudness;
	private double liveness;
	private double dancebility;
	private double speechiness;
	private double acousticness;
	private double instrumer;
	
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getEnergy() {
		return energy;
	}
	public void setEnergy(String energy) {
		this.energy = energy;
	}
	public double getMood() {
		return mood;
	}
	public void setMood(double mood) {
		this.mood = mood;
	}
	public double getTempo() {
		return tempo;
	}
	public void setTempo(double tempo) {
		this.tempo = tempo;
	}
	public double getLoudness() {
		return loudness;
	}
	public void setLoudness(double loudness) {
		this.loudness = loudness;
	}
	public double getLiveness() {
		return liveness;
	}
	public void setLiveness(double liveness) {
		this.liveness = liveness;
	}
	public double getDancebility() {
		return dancebility;
	}
	public void setDancebility(double dancebility) {
		this.dancebility = dancebility;
	}
	public double getSpeechiness() {
		return speechiness;
	}
	public void setSpeechiness(double speechiness) {
		this.speechiness = speechiness;
	}
	public double getAcousticness() {
		return acousticness;
	}
	public void setAcousticness(double acousticness) {
		this.acousticness = acousticness;
	}
	public double getInstrumer() {
		return instrumer;
	}
	public void setInstrumer(double instrumer) {
		this.instrumer = instrumer;
	}
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		sb.append("Title: ").append(title);
		sb.append("Artist: ").append(artist);
		sb.append("Genre: ").append(genre);
		sb.append("Energy: ").append(energy);
		sb.append("Mood: ").append(mood);
		sb.append("Tempo: ").append(tempo);
		sb.append("Speechiness: ").append(speechiness);
		sb.append("Acousticness: ").append(acousticness);
		sb.append("Danceability: ").append(dancebility);
		sb.append("Instrumer: ").append(instrumer);
		sb.append("Loudness: ").append(loudness);
		
		return sb.toString();
	}	
}
