package edu.asu.data;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import DataStructures.ConcreteLinkedList;
import DataStructures.ILinkedList;

/**
 * This class allows you to read a CSV file. To use it, you must subclass it
 * and implement the two abstract methods (mapObject and getString)
 *
 * @param <T> type of the objects in the linked list
 */
public abstract class IO<T> {

	/**
	 * Abstract method that maps an array of string with an object of type T
	 * e.g. if you're reading a CSV file that has a line such as:
	 * 		Bob, 20, 4.0
	 * The line attribute will match accordingly:
	 * 		line[0] == "Bob"
	 * 		line[1] == "20"
	 * 		line[2] == "4.0"
	 * @param line a line in a CSV file
	 * @return an object T
	 */
	public abstract T mapObject(String[] line);
	
	/**
	 * This method must get an object from type T and convert it to a string in the format:
	 * 		"attribute 1, attribute 2, ..., attribute n" 
	 * This format matches a line in a CSV file format 
	 * e.g. for an object of type Student, the returned string should read:
	 * 		"Bob, 20, 4.0"
	 * @param object object to be converted
	 */
	public abstract String getString(T object);
	
	/**
	 * Reads a CSV file onto a linked list. Uses the mapObject method, which must be
	 * implemented on a concrete class, to map a line from the file onto a Java Object.
	 * 
	 * 
	 * YOU DON'T NEED TO CHANGE ANYTHING IN THIS METHOD
	 * @param fileName name of the CSV file
	 * @return the first element of the linked list
	 */
 	public ILinkedList<T> read(String fileName){
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
				
		ILinkedList<T> current = null, first = null; 

		try {

			br = new BufferedReader(new FileReader(fileName));
			br.readLine(); // Skipping title
			while ((line = br.readLine()) != null) {

		        // use comma as separator
				String[] splitLine = line.split(cvsSplitBy);
				T readValue = mapObject(splitLine);
				ILinkedList<T> node = new ConcreteLinkedList<T>(readValue);
				if(current == null){
					first = current = node;
				} else {
					current.setNext(node);
					current = node;
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return first;
	}
	
	/**
	 * Writes a linked list onto a CSV file. This method uses the getString method.
	 * YOU DON'T NEED TO CHANGE ANYTHING ON THIS METHOD.
	 * @param first the first node of the linked list
	 * @param fileName name of the file to be written to
	 */
	public void write(ILinkedList<T> first, String fileName){
		FileWriter f0 = null;
		try {
			f0 = new FileWriter(fileName);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String newLine = System.getProperty("line.separator");

		ILinkedList<T> current = first;
		while(current != null)
		{
			T s = current.getValue();
		    try {
				f0.write(getString(s) + newLine);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    current = current.next();
		}
		try {
			f0.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
