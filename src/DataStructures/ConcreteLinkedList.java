package DataStructures;

public class ConcreteLinkedList<T> implements ILinkedList<T> {
	
	private T value;
	private ILinkedList<T> next;
	private ILinkedList<T> prev;

	public ConcreteLinkedList(){}
	
	public ConcreteLinkedList(T value) {
		super();
		this.value = value;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	@Override
	public void setNext(ILinkedList<T> next) {
		this.next = next;
	}

	@Override
	public ILinkedList<T> next() {
		return this.next;
	}

	@Override
	public ILinkedList<T> prev() {
		return this.prev;
	}

	@Override
	public void setPrev(ILinkedList<T> prev) {
		this.prev = prev;
	}	
}
