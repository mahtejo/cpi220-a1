package DataStructures;

public interface ILinkedList<T> {
	
	public ILinkedList<T> next();
	public ILinkedList<T> prev();
	public T getValue();
	public void setNext(ILinkedList<T> next);
	public void setPrev(ILinkedList<T> prev);
	
}
